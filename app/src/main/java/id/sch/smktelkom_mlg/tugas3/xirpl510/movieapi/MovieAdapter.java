package id.sch.smktelkom_mlg.tugas3.xirpl510.movieapi;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Skaha_AM on 13-Feb-18.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {
    List<MovieModel> movieList;

    public MovieAdapter(List<MovieModel> movie) {
        movieList = movie;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_adapter, null);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MovieModel movie = movieList.get(position);
        holder.mTitle.setText(movie.getTitle());
        holder.mReleased.setText(movie.getReleased());
        holder.mGenre.setText(movie.getGenre());
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTitle;
        TextView mReleased;
        TextView mGenre;

        public ViewHolder(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.title_view);
            mReleased = itemView.findViewById(R.id.realesed_view);
            mGenre = itemView.findViewById(R.id.genre_view);
        }
    }
}
