package id.sch.smktelkom_mlg.tugas3.xirpl510.movieapi;

/**
 * Created by Skaha_AM on 13-Feb-18.
 */

public class MovieModel {
    private String title;
    private String released;
    private String genre;

    public MovieModel() {
    }

    public MovieModel(String title, String released, String genre) {
        this.title = title;
        this.released = released;
        this.genre = genre;
    }

    public String getTitle() {
        return title;
    }

    public String getReleased() {
        return released;
    }

    public String getGenre() {
        return genre;
    }

}
